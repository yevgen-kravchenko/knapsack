package edu.npl

case class Backpack(capacity: Int)

case class Item(weight: Int, price: Int)

case class Items(values: List[Item]) {

  def totalPrice(value: Array[Int]): Int = select(value).map(_.price).sum

  def totalWeight(value: Array[Int]): Int = select(value).map(_.weight).sum

  def select(value: Array[Int]): List[Item] = values.zipWithIndex
    .filter { case (_, idx) => value(idx) > 0 }
    .map(_._1)
}