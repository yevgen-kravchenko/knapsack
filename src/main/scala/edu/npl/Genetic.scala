package edu.npl

import edu.npl.Population.choose

import scala.util.Random


case class Individual(value: Array[Int]) {

  override def toString: String = value.mkString("")

  def fitness(backpack: Backpack, items: Items): Int = {
    val totalPrice = items.totalPrice(value)
    val totalWeight = items.totalWeight(value)
    if (totalWeight > backpack.capacity) 0 else totalPrice
  }

  def crossover(other: Individual): List[Individual] = {
    val mid = value.length / 2
    val firstHalf = value.slice(0, mid)
    val secondHalf = value.slice(mid, value.length)
    val otherFirstHalf = other.value.slice(0, mid)
    val otherSecondHalf = other.value.slice(mid, value.length)

    List(
      Individual(firstHalf ++ otherSecondHalf),
      Individual(otherFirstHalf ++ secondHalf)
    )
  }

  def mutate(): Individual = {
    val mutationIdx = Random.nextInt(value.length)
    val mutatedValue = value.zipWithIndex.map {
      case (v, idx) => if (idx == mutationIdx) v ^ 1 else v
    }
    Individual(mutatedValue)
  }

  override def equals(obj: Any): Boolean = obj match {
    case o: Individual => value sameElements o.value
    case _ => false
  }
}

object Individual {

  def generate(length: Int): Individual = {
    val value = Seq.fill(length)(Random.nextInt(2)).toArray
    Individual(value)
  }
}

case class Population(individuals: List[Individual]) {

  def selection(backpack: Backpack, items: Items): (Individual, Individual) = {
    val first = random()
    val second = random()
    val firstParent = choose(backpack, items, first, second)

    val third = random()
    val fourth = random()
    val secondParent = choose(backpack, items, third, fourth)
    (firstParent, secondParent)
  }

  def best(backpack: Backpack, items: Items): Individual =
    individuals.maxBy(_.fitness(backpack, items))

  override def toString: String = individuals.map(_.toString).mkString(",")

  private def random(): Individual =  individuals(Random.nextInt(individuals.size))
}

object Population {

  def initial(size: Int, length: Int): Population = {
    var individuals = List.empty[Individual]

    while (individuals.size < size) {
      val individual = Individual.generate(length)
      if (!individuals.contains(individual)) {
        individuals = individuals :+ individual
      }
    }

    Population(individuals)
  }

  private def choose(backpack: Backpack, items: Items, first: Individual, second: Individual): Individual = {
    if (first.fitness(backpack, items) > second.fitness(backpack, items)) first else second
  }
}

object Genetic {
  case class Config(initialPopulationSize: Int,
                    maxGenerations: Int,
                    crossoverRate: Int,
                    mutationRate: Int,
                    reproductionRate: Int)

  def mutation(individuals: List[Individual], mutationRate: Int): List[Individual] =
    if (Random.nextInt(100) < mutationRate) individuals.map(_.mutate()) else individuals

  def crossover(first: Individual, second: Individual, crossoverRate: Int): List[Individual] =
    if (Random.nextInt(100) < crossoverRate) first.crossover(second) else List.empty

  def newGeneration(backpack: Backpack, items: Items, population: Population, cfg: Config): Population = {
    var individuals = List.empty[Individual]

    while (individuals.length < population.individuals.length) {
      val (firstParent, secondParent) = population.selection(backpack, items)

      if (Random.nextInt(100) < cfg.reproductionRate) {
        individuals = individuals ++ List(firstParent, secondParent)
      }
      else {
        val children = crossover(firstParent, secondParent, cfg.crossoverRate)
        val mutated = mutation(children, cfg.mutationRate)
        individuals = individuals ++ mutated
      }
    }

    Population(individuals)
  }

  def algo(backpack: Backpack, items: Items, cfg: Config): List[Item] = {
    var population = Population.initial(cfg.initialPopulationSize, items.values.length)
    Range.inclusive(0, cfg.maxGenerations).foreach{
      idx =>
        population = newGeneration(backpack, items, population, cfg)
        println(s"Generation $idx -> $population")
    }

    val bestIndividual = population.best(backpack, items)
    println(s"Best individual across ${cfg.maxGenerations}  generations is: $bestIndividual")
    println(s"Price: ${items.totalPrice(bestIndividual.value)}")
    println(s"Weight: ${items.totalWeight(bestIndividual.value)}")

    items.select(bestIndividual.value)
  }
}
