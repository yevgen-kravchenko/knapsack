package edu.npl

object Main {

  def main(args: Array[String]): Unit = {
    val backpack = Backpack(15)
    val itemA = Item(7, 5)
    val itemB = Item(2, 4)
    val itemC = Item(1, 7)
    val itemD = Item(9, 2)
    val items = Items(List(itemA, itemB, itemC, itemD))

    val cfg = Genetic.Config(
      initialPopulationSize = 6,
      maxGenerations = 500,
      crossoverRate = 50,
      mutationRate = 2,
      reproductionRate = 30
    )
    val result = Genetic.algo(backpack, items, cfg)
    result.foreach(i => println(s"Item weight=${i.weight} price=${i.price}"))
  }
}
